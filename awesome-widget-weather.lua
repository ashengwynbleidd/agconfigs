local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
require("main.variables")
require("deco.theme")

-- Weather Widget --

weather = wibox.widget {
    widget = wibox.container.margin,
    margins = 2,
    {
        widget = wibox.container.background,
        shape = gears.shape.rounded_bar,
        bg = background_light,
        {
            layout = wibox.layout.fixed.horizontal,
            asep,
            {
                widget = wibox.widget.imagebox(icons .. "widget/weather.svg")
            },
            asep,
            {
                widget = wibox.container.margin,
                margins = 2,
                {
                    widget = wibox.container.background,
                    fg = foreground,
                    {
                        widget = awful.widget.watch(scripts .. "displayweather -c",1800)
                    }
                }
            },
            sep,
            {
                widget = wibox.container.margin,
                margins = 2,
                {
                    widget = wibox.container.background,
                    fg = foreground_dark,
                    {
                        widget = awful.widget.watch(scripts .. "displayweather -h",1800)
                    }
                }
            },
            sep,
            {
                widget = wibox.container.margin,
                margins = 2,
                {
                    widget = wibox.container.background,
                    fg = foreground_dark,
                    {
                        widget = awful.widget.watch(scripts .. "displayweather -l",1800)
                    }
                }
            },
            sep
        }
    }
}
