import XMonad
import System.IO (hPutStrLn)
import System.Exit(exitSuccess)
import qualified XMonad.StackSet as W
-- importing actions
import XMonad.Actions.CycleWS(nextWS, prevWS)
import XMonad.Actions.Promote
-- importing layouts & layout modifiers
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimpleFloat
import XMonad.Layout.MultiToggle
import XMonad.Layout.Reflect
import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
-- importing hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
-- importing utilities
import XMonad.Util.EZConfig(removeKeysP, additionalKeysP)
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.SpawnOnce

------------------------------------------------------------------------

myFocusFollowsMouse = True
myClickJustFocuses = False
myModMask = mod4Mask
myWorkspaces = [" 1 "," 2 "," 3 "," 4 "," 5 "," 6 "," 7 "," 8 "," 9 "]
myXmonadR = "xmonad --recompile && xmonad --restart"
myNormColor   = "#242424"
myFocusColor  = "#665c54"
myBorderWidth = 2

------------------------------------------------------------------------
-- Layouts:

myLayout = avoidStruts
            $ mkToggle (single REFLECTX)
            $ mkToggle (single REFLECTY)
            $  tall ||| full ||| float
    where
        tall  = renamed [Replace "Tall"] $ wmGaps $ ResizableTall nmaster delta ratio []
        full  = renamed [Replace "Full"] $ Full
        float = renamed [Replace "Float"] $ simpleFloat
        wmGaps  = spacingRaw False (Border 4 4 4 4) True (Border 4 4 4 4) True
        nmaster = 1
        ratio   = 1/2
        delta   = 1/100

------------------------------------------------------------------------
-- Window rules:

myManageHook = composeAll
    [
        className =? "error"                --> doFloat,
        className =? "dialog"               --> doFloat,
        className =? "splash"               --> doFloat,
        className =? "toolbar"              --> doFloat,
        className =? "confirm"              --> doFloat,
        className =? "download"             --> doFloat,
        className =? "notification"         --> doFloat,
        className =? "file_progress"        --> doFloat,
        className =? "gnome-control-center" --> doFloat,
        className =? "org.gnome.Software"   --> doFloat,
        className =? "org.gnome.Nautilus"   --> doFloat,
        className =? "gnome-calculator"     --> doFloat,
        className =? "Gnome-tweaks"         --> doFloat,
        className =? "Gnome-disks"          --> doFloat,
        className =? "fragments"            --> doFloat,
        className =? "shortwave"            --> doFloat,
        className =? "amberol"              --> doFloat,
        className =? "antimicrox"           --> doFloat,
        className =? "corectrl"             --> doFloat,
        className =? "gradience"            --> doFloat,
        className =? "mpv"                  --> doFloat,
        (className =? "librewolf" <&&> resource =? "Dialog")  --> doFloat,
        (className =? "librewolf" <&&> resource =? "Places")  --> doFloat,
        (className =? "librewolf" <&&> resource =? "Browser") --> doFloat,
        (className =? "firefox"   <&&> resource =? "Dialog")  --> doFloat,
        (className =? "firefox"   <&&> resource =? "Places")  --> doFloat,
        (className =? "firefox"   <&&> resource =? "Browser") --> doFloat,
        resource  =? "desktop_window" --> doIgnore
        ]

------------------------------------------------------------------------
-- Event handling:

myEventHook = mempty

------------------------------------------------------------------------
-- Startup hook:

myStartupHook = do
    spawnOnce "feh --bg-fill ~/Pictures/backgrounds/wyzweq.jpg"
    -- spawnOnce "blueman-applet"
    -- spawnOnce "nm-applet"
    -- spawnOnce "redshift"
    -- spawnOnce "picom"

------------------------------------------------------------------------
-- Keybindings:

myKeys =
    [
        ("M-S-q",           io exitSuccess),
        ("M-S-r",           spawn myXmonadR),
        ("M-q",             kill),
        ----- Changing Workspace -----
        ("M-C-<Right>",     nextWS),
        ("M-C-<Left>",      prevWS),
        ----- Changing focused window -----
        ("M1-<Tab>",        windows W.focusDown),
        ("M1-S-<Tab>",      windows W.focusUp),
        ----- Resizing layout windows -----
        ("M-<Right>",       sendMessage Expand),
        ("M-<Left>",        sendMessage Shrink),
        ("M-<Down>",        sendMessage MirrorExpand),
        ("M-<Up>",          sendMessage MirrorShrink),
        ----- Rearranging layout windows -----
        ("M-<Space>",       sendMessage $ Toggle REFLECTX),
        ("M-<Tab>",         sendMessage NextLayout),
        ("M-f",             withFocused $ windows . W.sink),
        ("M-<Backspace>",   windows W.swapMaster),
        ("M-S-<Down>",      windows W.swapDown),
        ("M-S-<Up>",        windows W.swapUp),
        ----- Spawning Applications -----
        ("M-S-<Return>",    spawn "rofi -show drun"),
        ("M-<Return>",      spawn "alacritty"),
        ----- Volume Controls -----
        ("<XF86AudioMute>",        spawn "amixer set Master toggle"),
        ("<XF86AudioLowerVolume>", spawn "amixer set Master 1%-"),
        ("<XF86AudioRaiseVolume>", spawn "amixer set Master 1%+")
    ]
    ++
    [ ("M-" ++ [index],   windows $ W.greedyView workspace)
                | (workspace, index) <- zip myWorkspaces ['1'..'9'] ]
    ++
    [ ("M-S-" ++ [index], windows (W.shift workspace)
                            >> windows (W.greedyView workspace))
                | (workspace, index) <- zip myWorkspaces ['1'..'9'] ]

-- Default Keybindings: (Removed)

myKeysR =
    [
        "M-<Space>",
        "M-p",
        "M-S-p",
        "M-S-c",
        "M-j",
        "M-k",
        "M-S-j",
        "M-S-k",
        "M-h",
        "M-l",
        "M-S-/",
        "M-,",
        "M-."
    ]
    ++ ["M-" ++ [index] | index <- ['1'..'9']]
    ++ ["M-S-" ++ [index] | index <- ['1'..'9']]

------------------------------------------------------------------------

main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar ~/.config/xmobar/xmobarrc"
    xmonad $ docks def
        {
            modMask            = myModMask,
            focusFollowsMouse  = myFocusFollowsMouse,
            clickJustFocuses   = myClickJustFocuses,
            workspaces         = myWorkspaces,
            borderWidth        = myBorderWidth,
            normalBorderColor  = myNormColor,
            focusedBorderColor = myFocusColor,
            layoutHook         = myLayout,
            manageHook         = myManageHook,
            handleEventHook    = myEventHook,
            startupHook        = myStartupHook,
            logHook            = dynamicLogWithPP xmobarPP
                {
                    ppOutput          = hPutStrLn xmproc,
                    ppCurrent         = xmobarColor "#fe8019" "",
                    ppVisible         = xmobarColor "#ffffff" "",
                    ppHidden          = xmobarColor "#ffffff" "",
                    ppHiddenNoWindows = xmobarColor "#665c54" "",
                    ppTitle           = xmobarColor "#ffffff" "" . shorten 30,
                    ppUrgent          = xmobarColor "#ff0000" "" . wrap "!" "!",
                    ppExtras          = [],
                    ppOrder           = \(ws:_) -> [ws]
                }
        }`removeKeysP` myKeysR `additionalKeysP` myKeys
