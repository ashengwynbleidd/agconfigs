import os
from libqtile import hook
from subprocess import run

script = os.path.expanduser('~/.agscripts/')
background = os.path.expanduser('~/Pictures/backgrounds/wyzweq.jpg')
icondir = os.path.expanduser('~/Pictures/icons/qtile/')
    
# @hook.subscribe.startup_once
# def start_once():
#     run(["nm-applet"])
#     run(["blueman-applet"])
#     run(["redshift"])
#     run(["picom"])

class Commands:

    def get_memory(self):
        my_memory = run([script+'displaystats', '-m'],
                        capture_output=True, text=True)
        return my_memory.stdout

    def get_memoryp(self):
        my_memoryp = run([script+'displaystats', '-mp'],
                         capture_output=True, text=True)
        return my_memoryp.stdout

    def get_cputemp(self):
        my_cputemp = run([script+'displaystats', '-c'],
                         capture_output=True, text=True)
        return my_cputemp.stdout

    def get_cpurpm(self):
        my_cputemp = run([script+'displaystats', '-cf'],
                         capture_output=True, text=True)
        return my_cputemp.stdout

    def get_gputemp(self):
        my_gputemp = run([script+'displaystats', '-g'],
                         capture_output=True, text=True)
        return my_gputemp.stdout

    def get_gpurpm(self):
        my_gputemp = run([script+'displaystats', '-gf'],
                         capture_output=True, text=True)
        return my_gputemp.stdout

    def get_volume(self):
        my_volume = run([script+'displaystats', '-v'],
                        capture_output=True, text=True)
        return my_volume.stdout

    def get_weather_current(self):
        my_temp = run([script+'displayweather', '-c'],
                      capture_output=True, text=True)
        return my_temp.stdout

    def get_weather_high(self):
        my_temp = run([script+'displayweather', '-h'],
                      capture_output=True, text=True)
        return my_temp.stdout

    def get_weather_low(self):
        my_temp = run([script+'displayweather', '-l'],
                      capture_output=True, text=True)
        return my_temp.stdout

commands = Commands()
