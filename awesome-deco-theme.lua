local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = require("beautiful.xresources").apply_dpi

beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

sep = wibox.widget.textbox("  ")
asep = wibox.widget.textbox(" ")
foreground = "#ffffff"
foreground_dark = "#7c6f64" --"#928374"
background_lighter = "#665c54"
background_light = "#242424"
background = "#161616"
wallpaper = os.getenv("HOME") .. "/Pictures/backgrounds/wyzweq.jpg"

beautiful.wallpaper = gears.wallpaper.maximized(wallpaper)
beautiful.font      = "JetBrains Mono Bold 11"
beautiful.fg_normal = foreground_dark
beautiful.fg_focus  = foreground
beautiful.bg_normal = background
beautiful.bg_focus  = background_light

beautiful.useless_gap   = dpi(6)
beautiful.border_width  = dpi(2)
beautiful.border_focus  = background_lighter
beautiful.border_normal = background_light

icons = os.getenv("HOME") .. "/Pictures/icons/awesome/"

-- Menu --
beautiful.awesome_icon = icons .. "awesome.png"
beautiful.menu_submenu_icon = icons .. "submenu.png"
beautiful.menu_height = dpi(42)
beautiful.menu_width = dpi(180)

-- Layouts --
beautiful.layout_tile      = icons .. "layout/tile.png"
beautiful.layout_floating  = icons .. "layout/floating.png"
beautiful.layout_max       = icons .. "layout/max.png"
beautiful.layout_magnifier = icons .. "layout/magnifier.png"

-- Titlebar --
beautiful.titlebar_floating_button_focus_active     = icons .. "titlebar/floating_focus_active.png"
beautiful.titlebar_floating_button_focus_inactive   = icons .. "titlebar/floating_focus_inactive.png"
beautiful.titlebar_floating_button_normal_active    = icons .. "titlebar/floating_normal_active.png"
beautiful.titlebar_floating_button_normal_inactive  = icons .. "titlebar/floating_normal_inactive.png"

beautiful.titlebar_ontop_button_focus_active        = icons .. "titlebar/ontop_focus_active.png"
beautiful.titlebar_ontop_button_focus_inactive      = icons .. "titlebar/ontop_focus_inactive.png"
beautiful.titlebar_ontop_button_normal_active       = icons .. "titlebar/ontop_normal_active.png"
beautiful.titlebar_ontop_button_normal_inactive     = icons .. "titlebar/ontop_normal_inactive.png"

beautiful.titlebar_sticky_button_focus_active       = icons .. "titlebar/sticky_focus_active.png"
beautiful.titlebar_sticky_button_focus_inactive     = icons .. "titlebar/sticky_focus_inactive.png"
beautiful.titlebar_sticky_button_normal_active      = icons .. "titlebar/sticky_normal_active.png"
beautiful.titlebar_sticky_button_normal_inactive    = icons .. "titlebar/sticky_normal_inactive.png"

beautiful.titlebar_maximized_button_focus_active    = icons .. "titlebar/maximized_focus_active.png"
beautiful.titlebar_maximized_button_focus_inactive  = icons .. "titlebar/maximized_focus_inactive.png"
beautiful.titlebar_maximized_button_normal_active   = icons .. "titlebar/maximized_normal_active.png"
beautiful.titlebar_maximized_button_normal_inactive = icons .. "titlebar/maximized_normal_inactive.png"

beautiful.titlebar_close_button_focus               = icons .. "titlebar/close_focus.png"
beautiful.titlebar_close_button_normal              = icons .. "titlebar/close_normal.png"

-- Taglis --
beautiful.taglist_squares_sel       = icons .. "square_sel.png"
beautiful.taglist_squares_sel_empty = icons .. "square_sel.png"
beautiful.taglist_squares_unsel     = icons .. "square_unsel.png"
beautiful.taglist_fg_focus          = foreground
beautiful.taglist_fg_occupied       = foreground
beautiful.taglist_fg_empty          = foreground_dark
beautiful.taglist_bg_focus          = background_light
beautiful.taglist_bg_occupied       = background
beautiful.taglist_bg_empty          = background
beautiful.taglist_spacing           = dpi(2)
beautiful.taglist_shape = function (cr, width, height)
    gears.shape.rounded_rect(cr, width, height, 8)
end

-- Tasklist --
beautiful.tasklist_shape = gears.shape.rounded_rect
-- beautiful.tasklist_disable_task_name = true
-- beautiful.tasklist_disable_icon = true
beautiful.tasklist_icon_size = dpi(14)

-- Systray --
beautiful.systray_icon_spacing = dpi(8)
