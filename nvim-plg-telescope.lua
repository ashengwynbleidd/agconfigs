-- telescope

local actions = require('telescope.actions')
local builtin = require('telescope.builtin')

require('telescope').setup {
  defaults = {
    layout_config = {
      horizontal = {
        prompt_position = 'top',
        height = 0.75,
        width = 0.9,
      }
    },
    mappings = {
      i = {
        ['<CR>'] = actions.select_default,
        ['<C-Left>'] = actions.select_vertical,
        ['<C-Right>'] = actions.select_vertical,
        ['<C-Down>'] = actions.select_horizontal,
        ['<C-Up>'] = actions.select_horizontal,
      }
    }
  },
  pickers = {
    find_files = {
      hidden = true,
    }
  }
}

require('telescope').load_extension 'file_browser' 

vim.keymap.set('n', '<leader><Enter>', ':Telescope file_browser<CR>')
vim.keymap.set('n', '<leader><Space>', ':Telescope buffers<CR>')
vim.keymap.set('n', '<leader>b',       ':Telescope current_buffer_fuzzy_find<CR>')
vim.keymap.set('n', '<leader>f',       ':Telescope find_files<CR>')
vim.keymap.set('n', '<leader>o',       ':Telescope oldfiles<CR>')
vim.keymap.set('n', '<leader>h',       ':Telescope help_tags<CR>')
