from libqtile import layout
from libqtile.config import Match

layouts = [
    layout.MonadTall(
        align=0,
        new_client_position="bottom",
        border_focus="#665c54",
        border_normal="#282828",
        border_width=2,
        change_ratio=-0.02,
        margin=6,
        ratio=0.5,
        max_ratio=0.7,
        min_ratio=0.3,
    ),
    layout.Max(
        margin=[40, 80, 40, 80]
    ),
]

floating_layout = layout.Floating(
    border_focus="#50665b",
    border_normal="#282828",
    border_width=2,
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="error"),
        Match(wm_class="dialog"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="confirm"),
        Match(wm_class="download"),
        Match(wm_class="notification"),
        Match(wm_class="file_progress"),
        Match(wm_class="Dialog"),
        Match(wm_class="Places"),
        Match(wm_class="Browser"),
        Match(wm_class="gnome-control-center"),
        Match(wm_class="org.gnome.Software"),
        Match(wm_class="org.gnome.Nautilus"),
        Match(wm_class="gnome-calculator"),
        Match(wm_class="Gnome-tweaks"),
        Match(wm_class="Gnome-disks"),
        Match(wm_class="fragments"),
        Match(wm_class="shortwave"),  
        Match(wm_class="amberol"),
        Match(wm_class="antimicrox"),
        Match(wm_class="corectrl"),
        Match(wm_class="gradience"),
        Match(wm_class="mpv"),
    ],
)
