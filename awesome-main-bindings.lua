local gears = require("gears")
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
require("main.variables")

-- {{{ Mouse bindings
globalbuttons = gears.table.join(
    -- awful.button({}, 4, awful.tag.viewnext),
    -- awful.button({}, 5, awful.tag.viewprev),
    awful.button({}, 3, function() mainmenu:toggle() end)
)

root.buttons(globalbuttons)
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(

    -- hotkeys
    awful.key(
        { modkey, altkey, "Control" }, "p",
        function () os.execute("poweroff") end,
        { description = "turnoff pc", group = "hotkeys" }
    ),
    awful.key(
        { modkey, altkey, "Control" }, "r",
        function () os.execute("reboot") end,
        { description = "reboot pc", group = "hotkeys" }
    ),
    awful.key(
        {modkey, "Control" }, "p",
        function () awful.spawn.with_shell("pgrep -x picom && pkill -x picom || picom") end,
        { description = "toggle picom", group = "hotkeys" }
    ),
    awful.key(
        {modkey, "Control" }, "r",
        function () awful.spawn.with_shell("pgrep -x redshift && pkill -x redshift || redshift") end,
        { description = "toggle redshift", group = "hotkeys" }
    ),

    awful.key(
        {}, "XF86AudioLowerVolume",
        function () os.execute("wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%-") end,
        { description = "decrease volume by 1%", group = "hotkeys" }
    ),
    awful.key(
        {}, "XF86AudioRaiseVolume",
        function () os.execute("wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%+") end,
        { description = "increase volume by 1%", group = "hotkeys" }
    ),
    awful.key(
        {}, "XF86AudioMute",
        function () os.execute("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle") end,
        { description = "mute volume", group = "hotkeys" }
    ),
    awful.key(
        {}, "XF86Calculator",
        function () awful.spawn("gnome-calculator") end,
        { description = "launch calculator", group = "hotkeys" }
    ),
    -- awesome
    awful.key(
        { modkey, "Shift" }, "r", awesome.restart,
        { description = "reload awesome", group = "awesome" }
    ),
    awful.key(
        { modkey, "Shift" }, "q", awesome.quit,
        { description = "quit awesome", group = "awesome" }
    ),
    awful.key(
        { modkey, }, "h", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }
    ),
    awful.key(
        { modkey, }, "w", function() mymainmenu:show() end,
        { description = "show main menu", group = "awesome" }
    ),

    -- tags
    awful.key(
        { modkey, "Control" }, "Left", awful.tag.viewprev,
        { description = "view previous tag", group = "tags" }
    ),
    awful.key(
        { modkey, "Control" }, "Right", awful.tag.viewnext,
        { description = "view next tag", group = "tags" }
    ),
    awful.key(
        { modkey, }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tags" }
    ),

    -- launchers
    awful.key(
        { modkey, }, "Return", function() awful.spawn(terminal) end,
        { description = "launch terminal", group = "launchers" }
    ),
    awful.key(
        { modkey, "Shift" }, "Return", function() awful.spawn("rofi -show drun") end,
        { description = "launch rofi", group = "launchers" }
    ),

    -- layouts
    awful.key(
        { modkey, }, "Tab", function() awful.layout.inc(1) end,
        { description = "select next", group = "layouts" }
    ),
    awful.key(
        { modkey, "Shift" }, "Tab", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layouts" }
    ),

    -- clients
    awful.key(
        { altkey, "Shift" }, "Tab", function() awful.client.focus.byidx(-1) end,
        { description = "focus previous client", group = "clients" }
    ),
    awful.key(
        { altkey, }, "Tab", function() awful.client.focus.byidx(1) end,
        { description = "focus next client", group = "clients" }
    ),
    awful.key(
        { modkey, "Shift" }, "Down", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client", group = "clients" }
    ),
    awful.key(
        { modkey, "Shift" }, "Up", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client", group = "clients" }
    ),
    awful.key(
        { modkey, "Shift" }, "Right", function() awful.client.incwfact(0.02) end,
        { description = "increase client height factor", group = "clients" }
    ),
    awful.key(
        { modkey, "Shift" }, "Left", function() awful.client.incwfact(-0.02) end,
        { description = "decrease client height factor", group = "clients" }
    ),
    awful.key(
        { modkey, }, "Right", function() awful.tag.incmwfact(0.02) end,
        { description = "increase master width factor", group = "clients" }
    ),
    awful.key(
        { modkey, }, "Left", function() awful.tag.incmwfact(-0.02) end,
        { description = "decrease master width factor", group = "clients" }
    ),
    awful.key(
        { modkey, "Control" }, "n", function() awful.client.restore() end,
        { description = "restore minimized", group = "clients" }
    )
)

clientkeys = gears.table.join(
    awful.key(
        { modkey, }, "q", function(c) c:kill() end,
        { description = "close", group = "clients" }
    ),
    awful.key(
        { modkey, }, "f", awful.client.floating.toggle,
        { description = "toggle floating", group = "clients" }
    ),
    awful.key(
        { modkey, }, "BackSpace", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "clients" }
    ),
    awful.key(
        { modkey, }, "n", function(c) c.minimized = true end,
        { description = "minimize", group = "clients" }
    ),
    awful.key(
        { modkey, }, "s", function(c) c.sticky = not c.sticky end,
        { description = "toggle sticky", group = "clients" }
    ),
    awful.key(
        { modkey, }, "t", function(c) c.ontop = not c.ontop end,
        { description = "toggle ontop", group = "clients" }
    ),
    awful.key(
        { modkey, }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "toggle maximize", group = "clients" }
    ),
    awful.key(
        { modkey, "Shift" }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "clients" }
    )
)

-- Bind all key numbers to tags.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        awful.key(
            { modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tags" }
        ),
        awful.key(
            { modkey, "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "tags" }
        ),
        awful.key(
            { modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                        tag:view_only()
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tags" }
        ),
        awful.key(
            { modkey, "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tags" }
        )
    )
end

clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

root.keys(globalkeys)
-- }}}
