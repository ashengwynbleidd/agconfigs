from libqtile import widget
from qtile_custom import commands, icondir

widget_defaults = dict(
    font="JetBrains Mono Bold",
    fontsize=14,
    padding=3,
    background="#161616",
    foreground="#ffffff",
)
extension_defaults = widget_defaults.copy()

widgets = [
    widget.TextBox(text=" ", padding=0),
    widget.Image(filename=icondir+"calendar.svg", margin=2),
    widget.Clock(format="%a %b%d %Y"),
    widget.TextBox(text=" ", padding=0),
    widget.Image(filename=icondir+"clock.svg", margin=2),
    widget.Clock(format="%H:%M %p"),
    widget.TextBox(text=" ", padding=0),
    widget.Image(filename=icondir+"weather.svg", margin=2),
    widget.GenPollText(
        func=lambda: commands.get_weather_current(),
        update_interval=1800,
    ),
    widget.GenPollText(
        func=lambda: commands.get_weather_high(),
        update_interval=1800,
        foreground="#665c64",
    ),
    widget.GenPollText(
        func=lambda: commands.get_weather_low(),
        update_interval=1800,
        foreground="#665c64",
    ),
    widget.Spacer(),
    widget.CurrentLayoutIcon(scale=0.6),
    widget.GroupBox(
        fontsize=14,
        padding=9,
        highlight_method="line",
        highlight_color=["#161616", "#282828"],
        this_current_screen_border="#d56d0e",
        this_screen_border="#d56d0e",
        active="#ffffff",
        inactive="#665c54",
    ),
    widget.Spacer(),
    widget.Image(filename=icondir+"ram.svg", margin=2),
    widget.GenPollText(
        func=lambda: commands.get_memory(),
        update_interval=3,
    ),
    widget.GenPollText(
        func=lambda: commands.get_memoryp(),
        update_interval=3,
        foreground="#665c54",
    ),
    widget.TextBox(text="  ", padding=0),
    widget.Image(filename=icondir+"cpu.svg", margin=2),
    widget.GenPollText(
        func=lambda: commands.get_cputemp(),
        update_interval=3,
    ),
    widget.TextBox(text="  ", padding=0),
    widget.GenPollText(
        func=lambda: commands.get_cpurpm(),
        update_interval=3,
        foreground="#665c54",
    ),
    widget.TextBox(text="  ", padding=0),
    widget.Image(filename=icondir+"gpu.svg", margin=2,),
    widget.GenPollText(
        func=lambda: commands.get_gputemp(),
        update_interval=3,
    ),
    widget.TextBox(text="  ", padding=0),
    widget.GenPollText(
        func=lambda: commands.get_gpurpm(),
        update_interval=3,
        foreground="#665c54",
    ),
    widget.TextBox(text="  ", padding=0),
    widget.Image(filename=icondir+"volume.svg", margin=2),
    widget.GenPollText(
        func=lambda: commands.get_volume(),
        update_interval=3,
    ),
    widget.TextBox(text="  ", padding=0),
    # widget.Systray(),
    # widget.TextBox(text="  ", padding=0),
]
