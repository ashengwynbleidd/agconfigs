local awful = require("awful")
local beautiful = require("beautiful")
local menubar = require("menubar")
require("main.variables")

awesomemenu = {
    { "configs", editor_cmd .. " " .. awesome.conffile },
    { "restart", awesome.restart },
    { "quit", function() awesome.quit() end },
}

mainmenu = awful.menu({
    items = {
        { "awesome", awesomemenu, beautiful.awesome_icon },
        { "open terminal", terminal }
    }
})

menubar.utils.terminal = terminal