local awful = require("awful")
local beautiful = require("beautiful")

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            class = {
                "Nm-connection-editor",
                "org.gnome.Software",
                "org.gnome.Nautilus",
                "gnome-control-center",
                "Blueman-manager",
                "malcontent-control",
                "gnome-calculator",
                "Gnome-tweaks",
                "Gnome-disks",
                "fragments",
                "shortwave",
                "amberol",
                "antimicrox",
                "corectrl",
                "gradience",
                "mpv",
            },
            role = { "pop-up" }
        },
        properties = {
            floating = true,
            placement = awful.placement.centered
        }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = { type = { "normal", "dialog" }
        },
        properties = { titlebars_enabled = true }
    },

}
