from libqtile.config import EzKey, EzClick, EzDrag
from libqtile.lazy import lazy
from qtile_groups import groups

keys = [
    EzKey("M-S-q",          lazy.shutdown()),
    EzKey("M-S-r",          lazy.restart()),
    EzKey("M-r",            lazy.reload_config()),
    EzKey("M-q",            lazy.window.kill()),
    # ----- Changing focused window -----
    EzKey("A-<Tab>",        lazy.layout.next()),
    EzKey("A-<Tab>",        lazy.layout.down()),
    EzKey("A-S-<Tab>",      lazy.layout.up()),
    # ----- Resizing layout windows -----
    EzKey("M-<Right>",      lazy.layout.shrink_main()),
    EzKey("M-<Left>",       lazy.layout.grow_main()),
    EzKey("M-S-<Right>",    lazy.layout.shrink()),
    EzKey("M-S-<Left>",     lazy.layout.grow()),
    # ----- Rearranging layout windows -----
    EzKey("M-<space>",      lazy.layout.flip()),
    EzKey("M-<Tab>",        lazy.next_layout()),
    EzKey("M-f",            lazy.window.toggle_floating()),
    EzKey("M-<BackSpace>",  lazy.layout.swap_main()),
    EzKey("M-S-<Down>",     lazy.layout.shuffle_down()),
    EzKey("M-S-<Up>",       lazy.layout.shuffle_up()),
    # ----- Spawning Applications -----
    EzKey("M-S-<Return>",   lazy.spawn("rofi -show drun")),
    EzKey("M-<Return>",     lazy.spawn("alacritty")),
    # ----- Volume Controls -----
    EzKey("<XF86AudioMute>",        lazy.spawn("amixer set Master toggle")),
    EzKey("<XF86AudioLowerVolume>", lazy.spawn("amixer set Master 5%-")),
    EzKey("<XF86AudioRaiseVolume>", lazy.spawn("amixer set Master 5%+")),
    # EzKey("<XF86AudioMute>",        lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle")),
    # EzKey("<XF86AudioLowerVolume>", lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%-")),
    # EzKey("<XF86AudioRaiseVolume>", lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%+")),
]

# ----- Changing Workspace -----
for i in groups:
    keys.extend(
        [
            EzKey("M-%s" % i.name,
                  lazy.group[i.name].toscreen()),
            EzKey("M-S-%s" % i.name,
                  lazy.window.togroup(i.name, switch_group=True)),
            EzKey("M-C-<Right>",
                  lazy.screen.next_group()),
            EzKey("M-C-<Left>",
                  lazy.screen.prev_group()),
        ]   
    )

# ----- Floating controls -----
mouse = [
    EzDrag("M-1",  lazy.window.set_position_floating(),
           start=lazy.window.get_position()),
    EzDrag("M-3",  lazy.window.set_size_floating(),
           start=lazy.window.get_size()),
    EzClick("M-2", lazy.window.bring_to_front()),
]
