local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
require("deco.theme")

-- Clock Widget --

clock = wibox.widget {
    widget = wibox.container.margin,
    margins = 2,
    {
        widget = wibox.container.background,
        shape = gears.shape.rounded_bar,
        bg = background_light,
        {
            layout = wibox.layout.fixed.horizontal,
            asep,
            {
                widget = wibox.widget.imagebox(icons .. "widget/clock.svg")
            },
            {
                widget = wibox.container.margin,
                margins = 2,
                {
                    widget = wibox.container.background,
                    fg = foreground,
                    {
                        widget = awful.widget.watch("date +\"%H:%M %p\"",30)
                    }
                }
            },
            sep
        }
    }
}
