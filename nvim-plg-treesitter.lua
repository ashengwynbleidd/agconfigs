-- treesitter
require('nvim-treesitter.configs').setup {
   ensure_installed = {
      'bash',
      'yaml',
      'toml',
      'http',
      'scss',
      'css',
      'cpp',
      'c',
      'lua',
      'python',
      'vim',
      'ini',
      'diff',
  },
   highlight = { enable = true },
   indent = { enable = true },
 }
