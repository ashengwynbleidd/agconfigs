-- Setting options
-- vim.g.loaded_netrw = 1
-- vim.g.loaded_netrwPlugin = 1

vim.o.hlsearch = false
vim.o.incsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true

vim.o.smartindent = true
vim.o.expandtab = true
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.tabstop = 4

vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.cursorline = true
vim.wo.cursorcolumn = true
vim.wo.showbreak = '+++'
vim.wo.scrolloff = 10
vim.wo.signcolumn = 'yes'

vim.o.mouse = 'a'
vim.o.breakindent = true
vim.o.undofile = true

vim.o.updatetime = 250

vim.o.termguicolors = true
vim.cmd("colorscheme gruvbox")

vim.cmd("set spell")

-- Split orientations
vim.cmd("set splitbelow")
vim.cmd("set splitright")

-- Formatting on save
-- vim.cmd("autocmd BufWritePre * lua vim.lsp.buf.format()")

-- Highlight on yank
local highlight_group = vim.api.nvim_create_augroup(
  'YankHighlight', { clear = true }
)

vim.api.nvim_create_autocmd(
  'TextYankPost',
  {
    callback = function()
      vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = '*'
  }
)
