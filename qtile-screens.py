from libqtile import bar
from libqtile.config import Screen
from qtile_widgets import widgets
from qtile_custom import background

screens = [
    Screen(
        wallpaper=background,
        wallpaper_mode="fill",
        top=bar.Bar(
            widgets=widgets,
            size=42,
            # margin=[6, 6, 0, 6],
            border_width=[1, 2, 1, 2],
            border_color=[
                "#1d2021",
                "#161616",
                "#1d2021",
                "#161616"
            ],
        ),
    ),
]
