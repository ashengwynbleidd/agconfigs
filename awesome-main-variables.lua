scripts = os.getenv("HOME") .. "/.agscripts/"
editor = os.getenv("EDITOR") or "nvim"
terminal = "alacritty"
editor_cmd = terminal .. " -e " .. editor
modkey = "Mod4"
altkey = "Mod1"
