pcall(require, "luarocks.loader")

require("awful.autofocus")

require("main.errors")
require("deco.theme")
require("main.layouts")
require("main.menu")
require("deco.wibar")
require("main.bindings")
require("main.rules")
require("main.signals")
require("main.autostart")
