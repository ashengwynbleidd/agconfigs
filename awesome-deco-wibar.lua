local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
require("main.variables")

require("widget.calendar")
require("widget.clock")
require("widget.weather")
require("widget.memory")
require("widget.cpu")
require("widget.gpu")
require("widget.volume")
require("widget.systray")

require("deco.taglist")
require("deco.tasklist")
require("deco.wallpaper")

screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Tags of each screen
    awful.tag(
        { "1", "2", "3", "4", "5", "6", "7", "8", "9" },
        s,
        awful.layout.layouts[1]
    )

    -- We need one layoutbox per screen.
    s.layoutbox = awful.widget.layoutbox(s)
    s.layoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))

    -- Create a taglist widget

    s.taglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
        widget_template = {
            id = "background_role",
            widget = wibox.container.background,
            {
                widget = wibox.container.margin,
                left = 14,
                right = 14,
                top = 0,
                bottom = 0,
                {
                    layout = wibox.layout.fixed.horizontal,
                    {
                        id = "index_role",
                        widget = wibox.widget.textbox,
                    },
                    {
                        id = "icon_role",
                        widget = wibox.widget.imagebox,
                    },
                    {
                        id = "text_role",
                        widget = wibox.widget.textbox,
                    }
                }
            }
        }
    }

    -- Create a tasklist widget
    s.tasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        widget_template = {
            id = "background_role",
            widget = wibox.container.background,
            {
                widget = wibox.container.margin,
                margins = 2,
                {
                    layout = wibox.layout.fixed.horizontal,
                    {
                        widget = wibox.container.margin,
                        margins = 4,
                        {
                            id = "icon_role",
                            widget = wibox.widget.imagebox,
                        }
                    },
                    {
                        id = "text_role",
                        widget = wibox.widget.textbox,
                    }
                }
            }
        }
    }

    -- Create the wibox
    s.wibox = awful.wibar({screen = s, position = "top", height = 42})

    -- Add widgets to the wibox
    s.wibox:setup {
        widget = wibox.container.margin,
        margins = 2,
        {
            layout = wibox.layout.align.horizontal,
            expand = "none",
            {
                layout = wibox.layout.fixed.horizontal,
                calendar, clock, weather,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                s.layoutbox, asep, s.taglist, asep,
                -- s.tasklist
            },
            {
                layout = wibox.layout.fixed.horizontal,
                memory, cpu, gpu, volume, systray,
            },
        }
    }
end)