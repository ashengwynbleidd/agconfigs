from qtile_custom import commands
from qtile_groups import groups
from qtile_screens import screens
from qtile_widgets import widget_defaults
from qtile_layouts import layouts, floating_layout
from qtile_keymaps import keys, mouse

dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

auto_fullscreen = False
auto_minimize = False
focus_on_window_activation = "smart"
reconfigure_screens = True

wl_input_rules = None
wmname = "Qtile"
