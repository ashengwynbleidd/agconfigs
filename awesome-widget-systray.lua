local gears = require("gears")
local wibox = require("wibox")
require("deco.theme")

-- Systray Widget --

systray = wibox.widget {
    widget = wibox.container.margin,
    margins = 2,
    {
        widget = wibox.container.background,
        shape = gears.shape.rounded_bar,
        bg = background_light,
        {
            layout = wibox.layout.fixed.horizontal,
            asep,
            {
                widget = wibox.container.margin,
                margins = 6,
                {
                    widget = wibox.widget.systray
                },
            sep
            }
        }
    }
}