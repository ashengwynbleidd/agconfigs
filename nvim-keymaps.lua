-- Basic Keymaps
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Keymaps for better default experience
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

vim.keymap.set('n', '<C-s>', ":w<CR>")
vim.keymap.set('n', '<C-x>', ":q<CR>")
vim.keymap.set('n', '<C-c>', ":bd<CR>")
vim.keymap.set('n', '<leader><Tab>', "<C-w><C-p><CR>")
vim.keymap.set('n', '<leader>p', '"+p')
vim.keymap.set('v', '<leader>y', '"+y')
vim.keymap.set('n', 'dw', 'diw')
vim.keymap.set('n', 'yw', 'yiw')
vim.keymap.set('n', '<C-t>', ":term<CR>")
vim.keymap.set('n', '<C-f>', ":lua vim.lsp.buf.format()<CR>")
