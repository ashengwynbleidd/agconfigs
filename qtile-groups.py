from libqtile.config import Group

groups = [
    Group(name="1", label="1", layout="max"),
    Group(name="2", label="2", layout="monadtall"),
    Group(name="3", label="3", layout="monadtall"),
    Group(name="4", label="4", layout="monadtall"),
    Group(name="5", label="5", layout="monadtall"),
    Group(name="6", label="6", layout="monadtall"),
    Group(name="7", label="7", layout="monadtall"),
    Group(name="8", label="8", layout="monadtall"),
    Group(name="9", label="9", layout="monadtall"),
]
